import SwiftUI
import UIKit

extension Color {
    init(_ hex: UInt) {
        self.init(
            .sRGB,
            red: Double((hex & 0x00FF0000) >> 16) / 255,
            green: Double((hex & 0x0000FF00) >> 8) / 255,
            blue: Double(hex & 0x000000FF) / 255,
            opacity: Double((hex & 0xFF000000) >> 24) / 255
        )
    }
}

func toDp(_ s: CGFloat) -> CGFloat {
    return UIScreen.main.bounds.width * s / 375
}

extension Int {
    var dp: CGFloat { toDp(CGFloat(self)) }
}

extension Double {
    var dp: CGFloat { toDp(self) }
}

extension CGFloat {
    var dp: CGFloat { toDp(self) }
}
