//
//  Login.swift
//  DemoIosSwift
//
//  Created by 迷麟 on 2023/6/26.
//

import SwiftUI

enum Field: Hashable {
    case usernameField
    case passwordField
}

enum RequestError: Error {
    case error(String)
}

struct LoginRequest: Codable {
    let username: String
    let password: String
}

struct LoginResponse: Codable {
    let success: Bool
    let desc: String?
}

struct Login: View {
    @State private var username = ""
    @State private var password = ""
    @FocusState private var focusedField: Field?
    private var buttonDisabled: Bool { username.isEmpty || password.count < 6 }
    @State private var loginResponse: LoginResponse?
    
    var body: some View {
        GeometryReader { _ in
            VStack(alignment: .leading, spacing: 0) {
                Logo().frame(maxWidth: .infinity)
                
                TextInput(
                    text: $username,
                    placeholder: "请输入手机号/邮箱",
                    keyboardType: .numbersAndPunctuation,
                    submitLabel: .next,
                    field: .usernameField,
                    focusedField: _focusedField,
                    onSubmit: { focusedField = .passwordField }
                ) {}
                
                TextInput(
                    isSecure: true,
                    text: $password,
                    placeholder: "请输入登录密码",
                    submitLabel: .done,
                    field: .passwordField,
                    focusedField: _focusedField,
                    onSubmit: { clickLogin() }
                ) {
                    Divider()
                        .frame(height: 11.dp)
                        .padding([.leading, .trailing], 11.dp)
                    Tip(text: "忘记密码")
                }
                
                LoginButton(disabled: buttonDisabled) { clickLogin() }
                Tip(text: "手机验证码登录")
                    .padding(.leading, 32.dp)
                    .padding(.top, 16.dp)
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
            .overlay(alignment: .topLeading) { NavLeft() }
            .overlay(alignment: .topTrailing) { NavRight() }
            .overlay(alignment: .bottom) { Bottom() }
            .ignoresSafeArea(.keyboard, edges: .bottom)
            .alert(isPresented: Binding<Bool>(
                get: { loginResponse != nil },
                set: { if !$0 { loginResponse = nil} })
            ) {
                Alert(
                    title: Text((loginResponse!.success ? "登录成功" : loginResponse!.desc) ?? "未知错误"),
                    dismissButton: .default(Text("确定"))
                )
            }
        }
    }
    
    func clickLogin() {
        focusedField = nil
        
        if (buttonDisabled) {
            return
        }
        
        Task.init {
            do {
                loginResponse = try await request(username: username, password: password)
            } catch RequestError.error(let error) {
                loginResponse = LoginResponse(success: false, desc: error)
            }
        }
    }
    
    func request(username: String, password: String) async throws -> LoginResponse {
        guard let url = URL(string: "https://apifoxmock.com/m1/4102536-3740803-default/demo/login") else {
            throw RequestError.error("无效的请求url")
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let loginRequest = LoginRequest(username: username, password: password)
        guard let requestData = try? JSONEncoder().encode(loginRequest) else {
            throw RequestError.error("无效的请求参数")
        }
        request.httpBody = requestData
        
        let data: Data
        do {
            data = try await URLSession.shared.data(for: request).0
        } catch {
            throw RequestError.error("\(error.localizedDescription)")
        }
        
        guard let loginResponse = try? JSONDecoder().decode(LoginResponse.self, from: data) else {
            throw RequestError.error("无效的请求结果")
        }
        
        if !loginResponse.success {
            throw RequestError.error(loginResponse.desc ?? "未知错误")
        }
        
        return loginResponse
    }
}

struct NavLeft: View {
    var body: some View {
        Image("icon_back")
            .resizable()
            .frame(width: 9.dp, height: 15.dp)
            .offset(x: 16.dp, y: 19.dp)
    }
}

struct NavRight: View {
    var body: some View {
        Text("预约开通")
            .foregroundColor(Color(0xFF666666))
            .font(.system(size: 13.dp))
            .fontWeight(.semibold)
            .offset(x: -16.dp, y: 20.dp)
    }
}

struct Logo: View {
    var body: some View {
        Image("logo_sqb")
            .resizable()
            .frame(width: 150.dp, height: 47.5.dp)
            .padding(.top, 109.5.dp)
            .padding(.bottom, 45.dp)
    }
}

struct TextInput<Label>: View where Label: View {
    @State var isSecure = false
    @Binding var text: String
    var placeholder = ""
    var keyboardType = UIKeyboardType.default
    var submitLabel = SubmitLabel.return
    var field: Field? = nil
    @FocusState var focusedField: Field?
    var onSubmit: () -> Void = {}
    @ViewBuilder var label: () -> Label?
    
    @State private var eyeOpen = false
    
    @ViewBuilder var prompt: Text {
        Text(placeholder)
            .foregroundColor(Color(0xFF999999))
            .font(.system(size: 14.dp))
    }
    
    @ViewBuilder var textField: some View {
        if isSecure && !eyeOpen {
            SecureField("", text: $text,prompt: prompt)
        } else {
            TextField("", text: $text,prompt: prompt)
        }
    }
    
    var body: some View {
        HStack(spacing: 0) {
            textField
            .frame(height: 49.5.dp)
            .foregroundColor(Color.black)
            .font(.system(size: 18.dp))
            .focused($focusedField, equals: field)
            .keyboardType(keyboardType)
            .submitLabel(submitLabel)
            .onSubmit { onSubmit() }
            
            if (!text.isEmpty) {
                Button(action: { text = "" }) {
                    Image("icon_close")
                        .resizable()
                        .frame(width: 15.dp, height: 15.dp)
                }
                .buttonStyle(EmptyButtonStyle())
            }
            
            if (isSecure) {
                Button(action: { eyeOpen.toggle() }) {
                    Image(eyeOpen ? "icon_eye_1" : "icon_eye_0")
                        .resizable()
                        .frame(width: 15.dp, height: 15.dp)
                }
                .padding(.leading, 6.dp)
                .buttonStyle(EmptyButtonStyle())
            }
            
            label()
        }
        .padding([.leading, .trailing], 32.dp)
        .padding(.top, 15.dp)
        .overlay(alignment: .bottom) {
            Divider().padding([.leading, .trailing], 32.dp)
        }
    }
}

struct LoginButton: View {
    var disabled: Bool
    var action: () -> Void
    
    @State private var pressing: Bool = false
    
    var body: some View {
        Button(action: { action() }) {
            Text("登录")
                .foregroundColor(Color.white)
                .font(.system(size: 16.dp))
                .frame(maxWidth: .infinity, maxHeight: 44.dp)
                .background(disabled ? Color(0xFFf5e2b8) : pressing ? Color(0xFFb5802b) : Color(0xFFEE9E00))
                .cornerRadius(22.5.dp)
        }
        .padding([.leading, .trailing], 32.dp)
        .padding(.top, 31.5.dp)
        .disabled(disabled)
        .onLongPressGesture(
            minimumDuration: 0,
            perform: {},
            onPressingChanged: { pressing in
                self.pressing = pressing
            }
        )
        .buttonStyle(EmptyButtonStyle())
    }
}

struct Tip: View {
    var text: String
    
    var body: some View {
        Text(text)
            .foregroundColor(Color(0xFF666666))
            .font(.system(size: 13.dp))
    }
}

struct Third: View {
    var text: String
    var icon: String
    
    var body: some View {
        VStack {
            Image(icon)
                .resizable()
                .frame(width: 45.dp, height: 45.dp)
                .cornerRadius(22.5.dp)
                .overlay(RoundedRectangle(cornerRadius: 22.5.dp).stroke(Color(0xFFE5E5E5), lineWidth: 0.5.dp))
            Text(text)
                .foregroundColor(Color(0xFF999999))
                .font(.system(size: 11.dp))
        }
        .frame(maxWidth: .infinity)
    }
}

struct Bottom: View {
    var body: some View {
        HStack {
            Third(text: "微信登录", icon: "icon_wechat")
            Third(text: "支付宝登录", icon: "icon_alipay")
        }
        .padding([.leading, .trailing], 32.dp)
        .offset(y: -94.dp)
    }
}

struct EmptyButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
    }
}

struct Login_Previews: PreviewProvider {
    static var previews: some View {
        Login()
    }
}

